    //  slider services
    var swiper = new Swiper('.services__content .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        loopFillGroupWithBlank: true,
        breakpoints: {
            320: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                slidesPerColumn: 1
            },
            600: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 20
            },
            800: {
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 20
            },
            
            1060: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 30
            },
            
            1300: {
                slidesPerView: 5,
                spaceBetween: 30,
                slidesPerGroup: 5,
            }
            },
        navigation: {
        nextEl: '.services__content .swiper-button-next',
        prevEl: '.services__content .swiper-button-prev',
        },
    });
    
    // slider carousel
    var swiper = new Swiper('.carousel .swiper-container',{
        cssMode: false,
        effect: 'fade',
        speed: 1000,
        spaceBetween: 100,
        loop: true,
        autoplay: {
            delay: 3000,
            }, 
            slideShadows: true,
        mousewheel: false,
        keyboard: true,
        
    });
    // 
    // scroll
    // 
    $(window).on('scroll',function(){
        if($(window).scrollTop()){
        $('nav').addClass('navscroll');
        // $('header').append( "<p>Test</p>" );

        } else{
        $('nav').removeClass('navscroll');
        }
    })
    